//����������, ���������� �� �������� �� ����
#include<iostream>
#include<string>
#include<fstream>
#include<vector>
using namespace std;

class Calendar //����, ������� ��������
{
    int day;
    int month;
    int year;
    //�������� ����� �� �����
    public:
        Calendar();//����������� �� ������������
        Calendar(int d,int m,int y);//����������� � ���������
        int getD() const;
        int getM() const;
        int getY() const;
        void setD(int d);
        void setM(int m);
        void setY(int y);
};

Calendar::Calendar()//������������� �� �����������
{
    day=month=year=0;
}

Calendar::Calendar(int d, int m,int y)
{
    day=d;
    month=m;
    year=y;
}

int Calendar::getD() const//�������� �� ������� getD - ���������� � �� ���������� get � set
{
    return day;
}

int Calendar::getM() const
{
    return month;
}

int Calendar::getY() const
{
    return year;
}

void Calendar::setD(int d)
{
    day=d;
}

void Calendar::setM(int m)
{
    month=m;
}

void Calendar::setY(int y)
{
    year=y;
}

class Student//����, ������� �������
{
    string name;
    int facN;
    Calendar DateOfBirth;
    double avg;

    public:
        Student();//����������� �� ������������
        Student(string n, int fn, int dy, int mn, int yr, double a);//����������� � ���������
        string getName() const;
        int getFN() const;
        Calendar get_DoB() const;
        double getavg() const;
        void setName(string n);
        void setFN(int fn);
        void set_DoB(Calendar cl);
        void set_avg(double a);
        int age(Calendar cl, Student s);
};

Student::Student()
{
    name="";
    facN=0;
    Calendar DateOfBirth;
    avg=0;
}

Student::Student(string n, int fn, int d, int m, int y, double a):DateOfBirth(d,m,y)//�������� �� ������������ � ��������� �� Student � ����������� �� ����� �� ������������ � ��������� �� Calendar
{
    name=n;
    facN=fn;
    avg=a;
}

string Student::getName() const
{
    return name;
}

int Student::getFN() const
{
    return facN;
}

Calendar Student::get_DoB() const
{
    return DateOfBirth;
}

double Student::getavg() const
{
    return avg;
}

void Student::setName(string n)
{
    name=n;
}

void Student::setFN(int fn)
{
    facN=fn;
}

void Student::set_DoB(Calendar cl)
{
    DateOfBirth.setD(cl.getD());
    DateOfBirth.setM(cl.getM());
    DateOfBirth.setY(cl.getY());
}

void Student::set_avg(double a)
{
    avg=a;
}

int Student::age(Calendar cl,Student s)
{
    return cl.getY()-s.get_DoB().getY();
}

ostream& operator<<(ostream& out, const Student& s) //������������� �� ��������� << �� ����� �� ������ �� ���� Student
{
    cout << s.getName() << " " << s.getFN() << " " << s.get_DoB().getD() << "/" << s.get_DoB().getM() << "/" << s.get_DoB().getY() << " " << s.getavg();
    return out;
}

// �������� �� ��������� ������� ��� ���� � ��� file.txt
void saveToFile(Student s)
{
    ofstream file_obj;

    file_obj.open("file.txt", ios::trunc);
    file_obj.write((char*)&s, sizeof(s));

    file_obj.close();
}

// ������� �� ������ �� ����� � ��������� �� ����� �� ��������� �������
int readFromFile() {
    ifstream file_obj;
    file_obj.open("file.txt", ios::in);
    Student s;
    file_obj.read((char*)&s, sizeof(s));

    string studentName;

    while(!file_obj.eof()) {
        studentName = s.getName();

        file_obj.read((char*)&s, sizeof(s));
    }

    cout << "Student name read from file: " << studentName << std::endl;

    return 0;
}

int main() //������ ������� - ������ �������� �� ��������� ���
{
    Student s("Ivan", 12, 12, 6, 1994, 5.12); //��������� �� ����� �� ���� Student
    vector<Student> potok; //��������� �� �������� �� ���� Student � ��������� ����� (������)
    potok.push_back(s);
    Student s1("Petar", 13, 21, 5, 1984, 5.87);
    potok.push_back(s1);
    Student s2("Gosho", 14, 6, 6, 1995, 5.67);
    potok.push_back(s2);

    saveToFile(s1);
    readFromFile();

    Calendar c(4,11,2014); //��������� �� ����� �� ���� Calendar
    for(int i=0; i < potok.size(); i++) //��������� �� ������� � ������� �� �������� ����� 18 � 26 ������ � ��������� �� �������� �� ������
    {
        cout<<potok[i].getName()<<" is "<<potok[i].age(c,potok[i]) << endl;
        if(((c.getY() - potok[i].get_DoB().getY()) >= 18) && ((c.getY() - potok[i].get_DoB().getY()) <= 26))
        {
            cout << "Student between 18 and 26: " << potok[i] << endl;
        }
    }

    double temp_score = 0; //���������� �� ����������� �� ������� ��������� � ��������� �����
    string temp_name;
    for(int i=0; i < potok.size(); i++) //��������� �� ������� � ������� �� �������� � ���-����� �����
    {
        temp_score = potok[i].getavg();
        temp_name = potok[i].getName();
        if(potok[i].getavg() > temp_score)
        {
            temp_score = potok[i].getavg();
            temp_name = potok[i].getName();
        }
    }
    cout<<"Student with highest grade is: " << temp_name << " with " << temp_score << endl;
}

